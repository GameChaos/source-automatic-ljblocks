#include <sourcemod>
#include <sdktools>

float g_fJumpPosition[MAXPLAYERS + 1][3];
float g_fJEdge[MAXPLAYERS + 1];
float g_fLEdge[MAXPLAYERS + 1];
int g_iFramesOnGround[MAXPLAYERS + 1];
bool g_bLastJump[MAXPLAYERS + 1];
bool g_bJumpEnd[MAXPLAYERS + 1] = false;
bool g_bBlock[MAXPLAYERS + 1] = false;

#define JumpImpulse 301.993377
#define MaxDist 305
#define MinDist 100

public Plugin myinfo = 
{
	name = "Daeuaeoueou", 
	author = "GameChaos", 
	description = "eueoaueaoieiueuoa", 
	version = "0.1"
}

public Action OnPlayerRunCmd(int client, int &buttons, int &impulse, float vel[3], float angles[3], int &weapon, int &subtype, int &cmdnum, int &tickcount, int &seed, int mouse[2])
{
	if (IsValidClient(client))
	{
		if (GetEntityFlags(client) & FL_ONGROUND)
		{
			g_iFramesOnGround[client]++;
			
			if (g_iFramesOnGround[client] > 1)
			{
				if (buttons & IN_JUMP && !g_bLastJump[client])
				{
					GetClientAbsOrigin(client, g_fJumpPosition[client]);
					g_bJumpEnd[client] = false;
				}
			}
			else
			{
				if (!g_bJumpEnd[client])
				{
					JumpLand(client);
				}
			}
		}
		else if (GetEntityMoveType(client) != MOVETYPE_NOCLIP && GetEntityMoveType(client) != MOVETYPE_LADDER)
		{
			g_iFramesOnGround[client] = 0;
		}
		g_bLastJump[client] = !!(buttons & IN_JUMP);
	}
	return Plugin_Changed;
}

stock bool IsValidClient(client)
{
	return (client >= 1 && client <= MaxClients && IsValidEntity(client) && IsClientConnected(client) && IsClientInGame(client));
}

public JumpLand(int client)
{
	float position[3];
	int blockdist;
	
	GetClientAbsOrigin(client, position)
	blockdist = BlockDist(client, position);
	
	if (blockdist > MinDist && blockdist < MaxDist && g_bBlock[client])
	{
		PrintToChat(client, "[KZ] %i block | %f jumpoff | %f landing", blockdist, g_fJEdge[client], g_fLEdge[client]);
		PrintToConsole(client, "[KZ] %i block | %f jumpoff | %f landing", blockdist, g_fJEdge[client], g_fLEdge[client]);
	}
	g_bJumpEnd[client] = true;
	g_bBlock[client] = false;
}

stock float CalcDistance(float x, float y)
{
	return SquareRoot(x * x + y * y);
}

int BlockDist(int client, float position[3])
{
	int blockdist;
	float endedge[3];
	float startedge[3];
	
	for (int i = 0; i < 2; i++)
	{
		if (BlockDirection(g_fJumpPosition[client], position) == i)
		{
			float pos2[3];
			position[2] = g_fJumpPosition[client][2];
			
			pos2 = position;
			pos2[2] = g_fJumpPosition[client][2] + 1;
			pos2[i] += (g_fJumpPosition[client][i] - position[i]) / 2;
			TraceBlock(client, pos2, g_fJumpPosition[client], startedge);
			
			pos2 = g_fJumpPosition[client];
			pos2[2] += 1;
			pos2[i] += (position[i] - g_fJumpPosition[client][i]) / 2;
			TraceBlock(client, pos2, position, endedge);
			
			blockdist = RoundFloat(FloatAbs(endedge[i] - startedge[i]) + 32);
			g_fJEdge[client] = FloatAbs(g_fJumpPosition[client][i] - RoundFloat(startedge[i]));
			g_fLEdge[client] = FloatAbs(position[i] - RoundFloat(endedge[i]));
		}
	}
	return blockdist;
}

stock TraceBlock(int client, float pos2[3], float position[3], float result[3])
{
	float mins[3] =  { -16.0, -16.0, -1.0 };
	float maxs[3] =  { 16.0, 16.0, 0.0 };
	
	Handle trace = TR_TraceHullFilterEx(pos2, position, mins, maxs, MASK_SHOT, TraceEntityFilterPlayer);
	
	if(TR_DidHit(trace))
	{				
		TR_GetEndPosition(result, trace);
		g_bBlock[client] = true;
	}
	CloseHandle(trace); 
}

stock int BlockDirection(float jumpoff[3], float position[3])
{
	return FloatAbs(position[1] - jumpoff[1]) > FloatAbs(position[0] - jumpoff[0]);
}

public bool TraceEntityFilterPlayer(entity, any data)
{
	return entity > MAXPLAYERS;
}